#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <limits.h>

int* count_ready_steps_for_threads;
int count_steps_between_checks;
int is_stop;

int max_elem_in_array (const int* array, int count_elem) {
    int max = array[0];
    for (int i = 1; i < count_elem; i++) {
        if (max < array[i]) {
            max = array[i];
        }
    }
    return max;
}

void interrupt_handler (int sig) {
    if (sig != SIGINT) {
        return;
    }
    is_stop = 1;
}

void check_error (int status) {
    if (status != 0) {
        printf("Error. Status: %d\n", status);
        pthread_exit(&status);
    }
}

void* count_part_Pi (void* args) {
    int count_threads = ((int*) args)[0];
    int thread_id = ((int*) args)[1];

    double part_Pi = 0.0;

    long denominator = 1;
    for (int i = 0; i < thread_id; ++i) {
        denominator += 2;
    }

    int ready_steps = 0;
    int numerator = (thread_id % 2 == 0) ? 1 : -1;

    while (!is_stop) {
        for (int i = 0; i < count_steps_between_checks; ++i) {
            part_Pi += (double) numerator / (double) denominator;
            if (denominator < LONG_MAX - 2 * count_threads) {
                denominator += 2 * count_threads;
            }
            if (ready_steps > INT_MAX - count_threads) {
                ready_steps = (ready_steps % 2 == 0) ? 0 : 1;
            }
            ready_steps += count_threads;
            numerator = ((ready_steps + thread_id) % 2 == 0) ? 1 : -1;
        }
        count_ready_steps_for_threads[thread_id] = ready_steps;
    }

    int max_count_ready_steps = max_elem_in_array(count_ready_steps_for_threads, count_threads);
    if (ready_steps < max_count_ready_steps) {
        for (int i = 0; i < max_count_ready_steps - ready_steps; ++i) {
            part_Pi += (double) numerator / (double) denominator;
            if (denominator < LONG_MAX - 2 * count_threads) {
                denominator += 2 * count_threads;
            }
            if (ready_steps > INT_MAX - count_threads) {
                ready_steps = (ready_steps % 2 == 0) ? 0 : 1;
            }
            ready_steps += count_threads;
            numerator = ((ready_steps + thread_id) % 2 == 0) ? 1 : -1;
        }
    }

    double* ref_part_Pi = malloc(sizeof(double));
    *ref_part_Pi = part_Pi;
    return (void*) ref_part_Pi;
}

int main (int argc, char** argv) {
    if (argc != 2) {
        perror("Error. No argument or too many!\n");
        return 1;
    }
    int count_threads = (int) strtol(argv[1], NULL, 0);
    count_steps_between_checks = 10000000;

    pthread_t* threads = malloc(sizeof(pthread_t) * count_threads);
    int** args_threads = malloc(sizeof(int*) * count_threads);
    count_ready_steps_for_threads = malloc(sizeof(int) * count_threads);

    is_stop = 0;
    signal(SIGINT, interrupt_handler);

    for (int i = 0; i < count_threads; ++i) {
        args_threads[i] = malloc(sizeof(int) * 2);
        args_threads[i][0] = count_threads;
        args_threads[i][1] = i;

        int status = pthread_create(&threads[i], NULL, count_part_Pi, (void*) args_threads[i]);
        check_error(status);
    }

    double Pi = 0.0;
    for (int i = 0; i < count_threads; ++i) {
        double* part_Pi = NULL;
        int status = pthread_join(threads[i], (void**) &part_Pi);
        check_error(status);
        Pi += *part_Pi;
        free(part_Pi);
        free(args_threads[i]);
    }

    free(args_threads);
    free(threads);
    free(count_ready_steps_for_threads);

    printf("\nPi: %.50lf\n", Pi * 4);
    return 0;
}
