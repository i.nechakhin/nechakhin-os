#include <stdio.h>
#include <time.h>

int main () {
    time_t current_time = time(NULL);
    printf("%s\r\n", ctime(&current_time));
    printf("%.s\r\n", ctime(&current_time));
    return 0;
}
