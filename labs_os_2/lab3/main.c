#include <stdio.h>
#include <pthread.h>

void* print_strings (void* args) {
    char** array = (char**) args;
    for (int i = 0; array[i] != NULL; ++i) {
        printf("%s\n", array[i]);
    }
    return NULL;
}

void check_error (int status) {
    if (status != 0) {
        printf("Error. Status: %d\n", status);
        pthread_exit(&status);
    }
}

int main() {
    pthread_t thread1, thread2, thread3, thread4;
    char* array1[] = {"1", "2", "3", "4", "5", NULL};
    char* array2[] = {"6", "7", "8", "9", "10", "11", "12", NULL};
    char* array3[] = {"13", "14", "15", "16", NULL};
    char* array4[] = {"17", "18", "19", NULL};
    int status;
    status = pthread_create(&thread1, NULL, print_strings, array1);
    check_error(status);
    status = pthread_create(&thread2, NULL, print_strings, array2);
    check_error(status);
    status = pthread_create(&thread3, NULL, print_strings, array3);
    check_error(status);
    status = pthread_create(&thread4, NULL, print_strings, array4);
    check_error(status);
    pthread_exit(NULL);
}
