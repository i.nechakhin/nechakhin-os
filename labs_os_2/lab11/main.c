#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

pthread_mutex_t mutexes[3];

typedef enum Num_Mutex {
    START_MUTEX = 0,
    MUTEX1 = 1,
    MUTEX2 = 2
} Num_Mutex;

void* print_strings (void* args) {
    if (args != NULL) {
        return args;
    }

    pthread_mutex_lock(&mutexes[START_MUTEX]);
    for (int i = 0; i < 10; ++i) {
        pthread_mutex_lock(&mutexes[MUTEX2]);
        printf("Child Thread #%d\n", i);
        pthread_mutex_unlock(&mutexes[MUTEX2]);
        pthread_mutex_unlock(&mutexes[START_MUTEX]);

        pthread_mutex_lock(&mutexes[MUTEX1]);
        pthread_mutex_lock(&mutexes[START_MUTEX]);
        pthread_mutex_unlock(&mutexes[MUTEX1]);
    }
    pthread_mutex_unlock(&mutexes[START_MUTEX]);

    return NULL;
}

void check_error (int status) {
    if (status != 0) {
        printf("Error. Status: %d\n", status);
        pthread_exit(&status);
    }
}

int main() {
    int status;
    pthread_mutexattr_t attr;
    pthread_mutexattr_init(&attr);
    pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_ERRORCHECK);
    for (int i = 0; i < 3; ++i) {
        status = pthread_mutex_init(&mutexes[i], &attr);
        check_error(status);
    }
    pthread_t thread;

    pthread_mutex_lock(&mutexes[START_MUTEX]);
    status = pthread_create(&thread, NULL, print_strings, NULL);
    check_error(status);
    sleep(1);
    pthread_mutex_lock(&mutexes[MUTEX2]);
    pthread_mutex_unlock(&mutexes[START_MUTEX]);
    for (int i = 0; i < 10; ++i) {
        printf("Parent Thread #%d\n", i);
        pthread_mutex_lock(&mutexes[MUTEX1]);
        pthread_mutex_unlock(&mutexes[MUTEX2]);

        pthread_mutex_lock(&mutexes[START_MUTEX]);
        pthread_mutex_lock(&mutexes[MUTEX2]);
        pthread_mutex_unlock(&mutexes[START_MUTEX]);
        pthread_mutex_unlock(&mutexes[MUTEX1]);
    }
    pthread_mutex_unlock(&mutexes[MUTEX2]);

    pthread_join(thread, NULL);
    for (int i = 0; i < 3; ++i) {
        pthread_mutex_destroy(&mutexes[i]);
    }
    return 0;
}

/*

#include <stdio.h>
#include <pthread.h>

pthread_mutex_t mutexes[3];

typedef enum Num_Mutex {
    START_MUTEX = 0,
    MUTEX1 = 1,
    MUTEX2 = 2
} Num_Mutex;

void* print_strings (void* args) {
    if (args != NULL) {
        return args;
    }

    pthread_mutex_lock(&mutexes[START_MUTEX]);
    for (int i = 0; i < 10; ++i) {
        pthread_mutex_lock(&mutexes[MUTEX2]);
        printf("Child Thread #%d\n", i);
        pthread_mutex_unlock(&mutexes[MUTEX1]);
    }
    pthread_mutex_unlock(&mutexes[START_MUTEX]);

    return 0;
}

void check_error (int status) {
    if (status != 0) {
        printf("Error. Status: %d\n", status);
        pthread_exit(&status);
    }
}

int main (void) {
    int status;
    //pthread_mutexattr_t attr;
    //pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_ERRORCHECK);
    for (int i = 0; i < 3; ++i) {
        status = pthread_mutex_init(&mutexes[i], NULL);
        //status = pthread_mutex_init(&mutexes[i], &attr);
        check_error(status);
    }
    pthread_t thread;

    pthread_mutex_lock(&mutexes[START_MUTEX]);
    status = pthread_create(&thread, NULL, print_strings, NULL);
    check_error(status);
    pthread_mutex_lock(&mutexes[MUTEX2]);
    pthread_mutex_unlock(&mutexes[START_MUTEX]);
    for (int i = 0; i < 10; ++i) {
        pthread_mutex_lock(&mutexes[MUTEX1]);
        printf("Parent Thread #%d\n", i);
        pthread_mutex_unlock(&mutexes[MUTEX2]);
    }

    pthread_join(thread, NULL);
    for (int i = 0; i < 3; ++i) {
        pthread_mutex_destroy(&mutexes[i]);
    }
    return 0;
}
*/
