#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

void* print_message (void* args) {
    if (args != NULL) {
        return args;
    }
    while (1) {
        printf("Message. ");
    }
}

void handler_func (void* args) {
    if (args != NULL) {
        return;
    }
    printf("\nCancel me\n");
}

int main() {
    pthread_t thread;
    int status = pthread_create(&thread, NULL, print_message, NULL);
    if (status != 0) {
        printf("Error. Status: %d\n", status);
        pthread_exit(&status);
    }

    sleep(2);
    pthread_cleanup_push(handler_func, NULL);
    pthread_cleanup_pop(1);
    printf("\nCancel thread\n");
    pthread_cancel(thread);

    return 0;
}
