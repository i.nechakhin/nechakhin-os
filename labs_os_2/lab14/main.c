#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>

sem_t sem1, sem2;

void* print_strings (void* args) {
    if (args != NULL) {
        return args;
    }

    for (int i = 0; i < 10; ++i) {
        sem_wait(&sem1);
        printf("Child Thread #%d\n", i);
        sem_post(&sem2);
    }

    return NULL;
}

void check_error (int status) {
    if (status != 0) {
        printf("Error. Status: %d\n", status);
        pthread_exit(&status);
    }
}

int main () {
    sem_init(&sem1, 0, 0);
    sem_init(&sem2, 0, 1);
    pthread_t thread;
    int status = pthread_create(&thread, NULL, print_strings, NULL);
    check_error(status);

    for (int i = 0; i < 10; ++i) {
        sem_wait(&sem2);
        printf("Parent Thread #%d\n", i);
        sem_post(&sem1);
    }

    pthread_join(thread, NULL);
    sem_destroy(&sem1);
    sem_destroy(&sem2);
    return 0;
}
