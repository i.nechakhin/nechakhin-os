#include <stdio.h>
#include <pthread.h>

void* print_strings (void* args) {
    if (args != NULL) {
        return args;
    }
    for (int i = 0; i < 10; ++i) {
        printf("Child Thread #%d\n", i);
    }
    return NULL;
}

int main() {
    pthread_t thread;
    int status = pthread_create(&thread, NULL, print_strings, NULL);
    if (status != 0) {
        printf("Error. Status: %d\n", status);
        pthread_exit(NULL);
    }
    pthread_join(thread, NULL);

    for (int i = 0; i < 10; ++i) {
        printf("Parent Thread #%d\n", i);
    }

    return 0;
}
