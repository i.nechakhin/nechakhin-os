#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>

sem_t sem_a;
sem_t sem_b;
sem_t sem_c;
sem_t sem_module;
sem_t sem_widget;

void* provider_a () {
    while (1) {
        sleep(1);
        sem_post(&sem_a);
    }
}

void* provider_b () {
    while (1) {
        sleep(2);
        sem_post(&sem_b);
    }
}

void* provider_c () {
    while (1) {
        sleep(3);
        sem_post(&sem_c);
    }
}

void* provider_module () {
    while (1) {
        sem_wait(&sem_a);
        sem_wait(&sem_b);
        sem_post(&sem_module);
    }
}

void* provider_widget () {
    while (1) {
        sem_wait(&sem_c);
        sem_wait(&sem_module);
        sem_post(&sem_widget);
    }
}

void init_sem () {
    sem_init(&sem_a, 0, 0);
    sem_init(&sem_b, 0, 0);
    sem_init(&sem_c, 0, 0);
    sem_init(&sem_module, 0, 0);
    sem_init(&sem_widget, 0, 0);
}

void create_pthread (pthread_t* a, pthread_t* b, pthread_t* c, pthread_t* module, pthread_t* widget) {
    pthread_create(a, NULL, provider_a, NULL);
    pthread_create(b, NULL, provider_b, NULL);
    pthread_create(c, NULL, provider_c, NULL);
    pthread_create(module, NULL, provider_module, NULL);
    pthread_create(widget, NULL, provider_widget, NULL);
}

void get_value (int* count_a, int* count_b, int* count_c, int* count_module, int* count_widget) {
    sem_getvalue(&sem_a, count_a);
    sem_getvalue(&sem_b, count_b);
    sem_getvalue(&sem_c, count_c);
    sem_getvalue(&sem_module, count_module);
    sem_getvalue(&sem_widget, count_widget);
}

void print_result (int count_a, int count_b, int count_c, int count_module, int count_widget) {
    printf("A: %d\n", count_a);
    printf("B: %d\n", count_b);
    printf("C: %d\n", count_c);
    printf("Module: %d\n", count_module);
    printf("Widget: %d\n", count_widget);
}

void cancel_pthread (pthread_t a, pthread_t b, pthread_t c, pthread_t module, pthread_t widget) {
    pthread_cancel(a);
    pthread_cancel(b);
    pthread_cancel(c);
    pthread_cancel(module);
    pthread_cancel(widget);
}

void destroy_sem () {
    sem_destroy(&sem_a);
    sem_destroy(&sem_b);
    sem_destroy(&sem_c);
    sem_destroy(&sem_widget);
    sem_destroy(&sem_module);
}

int main () {
    init_sem();
    pthread_t a, b, c, module, widget;
    create_pthread(&a, &b, &c, &module, &widget);
    int count_a = 0;
    int count_b = 0;
    int count_c = 0;
    int count_module = 0;
    int count_widget = 0;

    while (count_widget < 5) {
        get_value(&count_a, &count_b, &count_c, &count_module, &count_widget);
        print_result(count_a, count_b, count_c, count_module, count_widget);
    }

    cancel_pthread(a, b, c, module, widget);
    destroy_sem();
    return 0;
}