#include <stdio.h>
#include <pthread.h>
#include <stdbool.h>

pthread_mutex_t mutex;
pthread_cond_t cond_val;
bool is_main_thread;

void* print_strings (void* args) {
    if (args != NULL) {
        return args;
    }

    for (int i = 0; i < 10; ++i) {
        pthread_mutex_lock(&mutex);
        while (is_main_thread) {
            pthread_cond_wait(&cond_val, &mutex);
        }
        printf("Child Thread #%d\n", i);
        is_main_thread = 1;
        pthread_mutex_unlock(&mutex);
        pthread_cond_signal(&cond_val);
    }

    return NULL;
}

void check_error (int status) {
    if (status != 0) {
        printf("Error. Status: %d\n", status);
        pthread_exit(&status);
    }
}

int main (void) {
    pthread_mutex_init(&mutex, NULL);
    pthread_cond_init(&cond_val, NULL);
    is_main_thread = 1;
    pthread_t thread;
    int status = pthread_create(&thread, NULL, print_strings, NULL);
    check_error(status);
    for (int i = 0; i < 10; ++i) {
        pthread_mutex_lock(&mutex);
        while (!is_main_thread) {
            pthread_cond_wait(&cond_val, &mutex);
        }
        printf("Parent Thread #%d\n", i);
        is_main_thread = 0;
        pthread_mutex_unlock(&mutex);
        pthread_cond_signal(&cond_val);
    }

    pthread_join(thread, NULL);
    pthread_mutex_destroy(&mutex);
    pthread_cond_destroy(&cond_val);
    return 0;
}