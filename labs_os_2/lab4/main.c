#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

void* print_message (void* args) {
    if (args != NULL) {
        return args;
    }
    while (1) {
        printf("Message. ");
    }
}

int main() {
    pthread_t thread;
    int status = pthread_create(&thread, NULL, print_message, NULL);
    if (status != 0) {
        printf("Error. Status: %d\n", status);
        pthread_exit(&status);
    }

    sleep(2);
    printf("\nCancel thread\n");
    pthread_cancel(thread);

    return 0;
}
