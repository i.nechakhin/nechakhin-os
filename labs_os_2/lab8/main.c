#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

void* count_part_Pi (void* args) {
    int count_threads = ((int*) args)[0];
    int thread_id = ((int*) args)[1];
    int count_steps_per_thread = ((int*) args)[2];

    int denominator = 1;
    for (int i = 0; i < thread_id; ++i) {
        denominator += 2;
    }
    int cur_num = thread_id;
    int numerator = (cur_num % 2 == 0) ? 1 : -1;

    double part_Pi = 0.0;
    for (int i = 0; i < count_steps_per_thread; ++i) {
        part_Pi += (double) numerator / (double) denominator;
        denominator += 2 * count_threads;
        cur_num += count_threads;
        numerator = (cur_num % 2 == 0) ? 1 : -1;
    }

    double* ref_part_Pi = malloc(sizeof(double));
    *ref_part_Pi = part_Pi;
    return (void*) ref_part_Pi;
}

void check_error (int status) {
    if (status != 0) {
        printf("Error. Status: %d\n", status);
        pthread_exit(&status);
    }
}

int main (int argc, char** argv) {
    const int MIN_COUNT_STEPS = 10000000;
    if (argc != 2) {
        perror("Error. No argument or too many!\n");
        return 1;
    }
    int count_threads = (int) strtol(argv[1], NULL, 0);
    int global_count_steps = count_threads;
    while (global_count_steps < MIN_COUNT_STEPS) {
        global_count_steps += count_threads;
    }
    int count_steps_per_thread = global_count_steps / count_threads;

    pthread_t* threads = malloc(sizeof(pthread_t) * count_threads);
    int** args_threads = malloc(sizeof(int*) * count_threads);
    for (int i = 0; i < count_threads; ++i) {
        args_threads[i] = malloc(sizeof(int) * 3);
        args_threads[i][0] = count_threads;
        args_threads[i][1] = i;
        args_threads[i][2] = count_steps_per_thread;
        int status = pthread_create(&threads[i], NULL, count_part_Pi, (void*) args_threads[i]);
        check_error(status);
    }

    double Pi = 0.0;
    for (int i = 0; i < count_threads; ++i) {
        double* part_Pi = NULL;
        int status = pthread_join(threads[i], (void**) &part_Pi);
        check_error(status);
        Pi += *part_Pi;
        free(part_Pi);
        free(args_threads[i]);
    }
    free(args_threads);
    free(threads);

    printf("Pi: %.50lf\n", Pi * 4);
    return 0;
}
