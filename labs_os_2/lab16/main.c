#include <stdio.h>
#include <semaphore.h>
#include <unistd.h>
#include <fcntl.h>

int main () {
    sem_t* sem1 = sem_open("sem1", O_CREAT | O_EXCL, S_IRUSR | S_IWUSR, 0);
    sem_t* sem2 = sem_open("sem2", O_CREAT | O_EXCL, S_IRUSR | S_IWUSR, 1);
    sem_unlink("sem1");
    sem_unlink("sem2");
    pid_t pid = fork();
    if (pid == -1) {
        printf("Error fork!\n");
        return 1;
    } else if (pid == 0) {
        for (int i = 0; i < 10; ++i) {
            sem_wait(sem1);
            printf("Child Thread #%d\n", i);
            sem_post(sem2);
        }
    } else {
        for (int i = 0; i < 10; ++i) {
            sem_wait(sem2);
            printf("Parent Thread #%d\n", i);
            sem_post(sem1);
        }
        sem_close(sem1);
        sem_close(sem2);
    }
}
