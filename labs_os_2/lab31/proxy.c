#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <poll.h>

#include "my_url.h"
#include "my_cache.h"

#define MAX_COUNT_CLIENTS 10
#define CACHE_SIZE 50
#define BUFFER_SIZE 1024
#define EMPTY -1
#define PROXY_PORT 2050

// For example:
// 1. literaturepage.com/read/doriangray-1.html
// 2. planetpublish.com/wp-content/uploads/2011/11/The_Picture_of_Dorian_Gray_NT.pdf

void create_poll (struct pollfd* poll_fds, int fd) {
    poll_fds[0].fd = fd;
    poll_fds[0].events = POLLIN;
}

void destroy_poll (struct pollfd* poll_fds) {
    if (poll_fds[0].fd < 0) {
        close(poll_fds[0].fd);
    }
}

int connect_to_server (const url_t* url) {
    printf("opening host...\n");
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd == -1) {
        perror("Error: socket error!");
        return -1;
    }

    struct hostent* hostent = gethostbyname(url->host);
    if (hostent == NULL) {
        perror("Error: gethostbyname error!");
        return -1;
    }

    for (int index = 0; hostent->h_addr_list[index] != NULL; ++index) {
        printf("host got!\n");
        struct sockaddr_in addr;
        memset(&addr, 0, sizeof(addr));
        addr.sin_port = htons(url->port);
        addr.sin_family = AF_INET;
        memcpy(&addr.sin_addr, hostent->h_addr_list[index], hostent->h_length);
        printf("host openned!\n");
        if (connect(socket_fd, (const struct sockaddr*) &addr, sizeof(struct sockaddr_in)) == -1) {
            continue;
        } else {
            return socket_fd;
        }
    }
    perror("Error: connect error!");
    return -1;
}

int find_empty_client_index (const int* clients) {
    int free_client_index = 0;
    while (free_client_index < MAX_COUNT_CLIENTS && clients[free_client_index] >= 0) {
        free_client_index++;
    }
    return free_client_index;
}

int accept_new_client (int listen_fd, int* clients) {
    int empty_client_index = find_empty_client_index(clients);
    clients[empty_client_index] = accept(listen_fd, NULL, NULL);
    char send_buffer[BUFFER_SIZE];
    snprintf(send_buffer, sizeof(send_buffer), "%s\r\n", "Welcome!");
    if (write(clients[empty_client_index], send_buffer, strlen(send_buffer)) == -1) {
        return 0;
    }
    return 1;
}

int try_accept_new_client (int listen_fd, int* clients, int count_connected_clients) {
    struct pollfd poll_fds[1];
    create_poll(poll_fds, listen_fd);
    if (count_connected_clients < MAX_COUNT_CLIENTS) {
        if ((poll(poll_fds, 1, 1)) == -1) {
            destroy_poll(poll_fds);
            return count_connected_clients;
        }
        if (poll_fds[0].revents & POLLIN) {
            if (!accept_new_client(listen_fd, clients)) {
                destroy_poll(poll_fds);
                return count_connected_clients;
            }
            count_connected_clients++;
        }
    }
    destroy_poll(poll_fds);
    return count_connected_clients;
}

void handle_client_disconnecting (int* clients, int* cache_to_client, size_t* count_sent_bytes, int client_index) {
    printf("Client %d disconnecting...\n", client_index);
    close(clients[client_index]);
    clients[client_index] = EMPTY;
    cache_to_client[client_index] = EMPTY;
    count_sent_bytes[client_index] = EMPTY;
}

char* create_request (const url_t* url) {
    char* request = malloc(sizeof(char) * BUFFER_SIZE);
    strcpy(request, "GET /");
    if (url->path != NULL) {
        strcat(request, url->path);
    }
    strcat(request, "\r\n");
    return request;
}

void send_request (int server_fd, const url_t* url) {
    char* request = create_request(url);
    if (write(server_fd, request, strlen(request)) == -1) {
        printf("Error: write request error\n");
    }
    free(request);
}

int try_find_in_cache (const cache_array_t* cache, const url_t* url, int* cache_to_client, int client_index) {
    int cache_index = (int) lookup_cache(cache, url);
    if (cache_index == -1) {
        printf("Page NOT found in cache!\n");
        return 0;
    } else {
        cache_to_client[client_index] = cache_index;
        printf("Page found in cache!\n");
        return 1;
    }
}

int main () {
    int listen_fd = socket(AF_INET, SOCK_STREAM, 0); // TCP/IP
    struct sockaddr_in server_addr;
    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY); //socket to join all net interface on host
    server_addr.sin_port = htons(PROXY_PORT);

    if (bind(listen_fd, (const struct sockaddr*) &server_addr, sizeof(server_addr)) == -1) {
        perror("Error: bind error\n");
        return 1;
    }

    if (listen(listen_fd, 5) == -1) {
        printf("Error: listen error\n");
        return 1;
    }

    int clients[MAX_COUNT_CLIENTS];
    int cache_to_client[MAX_COUNT_CLIENTS];
    size_t count_sent_bytes[MAX_COUNT_CLIENTS];
    for (int k = 0; k < MAX_COUNT_CLIENTS; k++) {
        clients[k] = EMPTY;
        cache_to_client[k] = EMPTY;
        count_sent_bytes[k] = EMPTY;
    }

    cache_array_t* cache = create_cache(CACHE_SIZE);
    int count_connected_clients = 0;
    while (1) {
        count_connected_clients = try_accept_new_client(listen_fd, clients, count_connected_clients);

        for (int client_index = 0; client_index < MAX_COUNT_CLIENTS; client_index++) {
            if (clients[client_index] < 2) {
                continue;
            }
            struct pollfd poll_fds[1];
            create_poll(poll_fds, clients[client_index]);
            if (poll(poll_fds, 1, 1) == -1) {
                destroy_poll(poll_fds);
                continue;
            }
            if (poll_fds[0].revents & POLLIN) {
                char urlBuffer[BUFFER_SIZE];
                size_t read_bytes = read(clients[client_index], &urlBuffer, BUFFER_SIZE);
                if (read_bytes) {
                    urlBuffer[read_bytes] = '\0';
                    printf("\"%s\"\n", urlBuffer);
                    if (strcmp(urlBuffer, "/exit") == 0) {
                        count_connected_clients--;
                        handle_client_disconnecting(clients, cache_to_client, count_sent_bytes, client_index);
                        continue;
                    }
                    url_t* url = create_url();
                    url = parse_url(url, urlBuffer);
                    if (url == NULL) {
                        printf("URL parsing error\n");
                        continue;
                    }
                    printf("Connecting socket...\n");
                    if (!try_find_in_cache(cache, url, cache_to_client, client_index)) {
                        int http_socket = connect_to_server(url);
                        if (http_socket == -1) {
                            if (write(clients[client_index], "Failed connect!\n", strlen("Failed connect!\n")) == -1) {
                                printf("Error: write error\n");
                            }
                            continue;
                        }

                        printf("Reading from http socket...\n");
                        send_request(http_socket, url);
                        write_to_cache(cache, http_socket, url);
                        size_t cache_index = cache->free_cache_index;
                        cache_to_client[client_index] = (int) cache_index;

                        printf("Cache read! Close http socket\n");
                        close(http_socket);
                    }
                    count_sent_bytes[client_index] = 0;
                    destroy_url(url);
                }
            }

            if (cache_to_client[client_index] != EMPTY) {
                size_t temp_sent_bytes = send_from_cache(cache, cache_to_client[client_index],
                                                         count_sent_bytes[client_index], clients[client_index]);
                if (temp_sent_bytes == (size_t) -1) {
                    handle_client_disconnecting(clients, cache_to_client, count_sent_bytes, client_index);
                } else if (temp_sent_bytes == (size_t) 0) {
                    printf("All sent. Remove cache to client\n");
                    cache_to_client[client_index] = EMPTY;
                    count_sent_bytes[client_index] = EMPTY;
                } else {
                    printf("Sent bytes\n");
                    count_sent_bytes[client_index] = temp_sent_bytes;
                }
            }
        }
    }
}