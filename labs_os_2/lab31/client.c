#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <poll.h>

#define BUFFER_SIZE 1024
#define PROXY_PORT 2050

void create_poll (struct pollfd* poll_fds, int fd1, int fd2) {
    poll_fds[0].fd = fd1;
    poll_fds[0].events = POLLIN;
    poll_fds[1].fd = fd2;
    poll_fds[1].events = POLLIN;
}

void destroy_poll (struct pollfd* poll_fds) {
    if (poll_fds[0].fd < 0) {
        close(poll_fds[0].fd);
    }
    if (poll_fds[1].fd < 0) {
        close(poll_fds[1].fd);
    }
}

int main (int argc, char* argv[]) {
    if (argc != 2) {
        printf("Invalid argument: received=%d, expected=%d\n", argc, 2);
        return 1;
    }

    struct sockaddr_in proxy_address;
    memset(&proxy_address, 0, sizeof(proxy_address));
    proxy_address.sin_family = AF_INET;
    proxy_address.sin_port = htons(PROXY_PORT);
    if (inet_pton(AF_INET, argv[1], &proxy_address.sin_addr) <= 0) {
        printf("Error: can't parse address=%s\n", argv[1]);
        return 1;
    }

    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0) {
        printf("Error: socket error\n");
        return 1;
    }
    if (connect(socket_fd, (struct sockaddr*) &proxy_address, sizeof(proxy_address)) < 0) {
        printf("Error: connect error\n");
        return 1;
    }

    struct pollfd poll_fds[2];
    create_poll(poll_fds, socket_fd, STDIN_FILENO);

    char buffer[BUFFER_SIZE];
    size_t count_read_bytes;
    while (1) {
        if (poll(poll_fds, 2, 1) == -1) {
            destroy_poll(poll_fds);
            break;
        }
        if (poll_fds[0].revents & POLLIN) {
            if ((count_read_bytes = read(socket_fd, buffer, sizeof(buffer) - 1)) > 0) {
                buffer[count_read_bytes] = 0;
                if (fputs(buffer, stdout) == EOF) {
                    printf("Error: puts error\n");
                }
            }
        }
        if (poll_fds[1].revents & POLLIN) {
            if ((count_read_bytes = read(STDIN_FILENO, buffer, BUFFER_SIZE - 1)) > 0) {
                buffer[count_read_bytes - 1] = 0;
                if (write(socket_fd, buffer, strlen(buffer)) == -1) {
                    printf("Error: write error\n");
                    break;
                }
                if (strcmp("/exit", buffer) == 0) {
                    close(socket_fd);
                    break;
                }
            }
        }
    }
    return 0;
}