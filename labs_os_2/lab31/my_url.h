#ifndef LAB31_MY_URL_H_
#define LAB31_MY_URL_H_

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

typedef struct url {
    char* host;
    char* path;
    int port;
} url_t;

url_t* create_url ();
url_t* parse_url (url_t* url, char* url_buffer);
int is_equal_url (const url_t* url1, const url_t* url2);
void copy_url (const url_t* url1, url_t* url2);
void destroy_url (url_t* url);

#endif