#include "my_url.h"

url_t* create_url () {
    url_t* url = malloc(sizeof(url_t));
    url->host = NULL;
    url->path = NULL;
    url->port = 80;
    return url;
}

url_t* parse_url (url_t* url, char* url_buffer) {
    size_t url_buffer_size = strlen(url_buffer);
    size_t start_port_index = 0;
    for (size_t i = 0; i < url_buffer_size; ++i) {
        if (url_buffer[i] == ':') {
            if (start_port_index) {
                destroy_url(url);
                return NULL;
            }
            start_port_index = i;
            char port[6] = {0}; // 5 + 1
            size_t port_str_index = i + 1;
            int port_index = 0;
            while (port_index < 5 && port_str_index < url_buffer_size && isdigit(url_buffer[port_str_index])) {
                port[port_index] = url_buffer[port_str_index];
                port_index++;
                port_str_index++;
            }
            url->port = (int) strtol(port, NULL, 0);
        }
        if (url_buffer[i] == '/') {
            if (i + 1 == url_buffer_size) {
                url_buffer[i] = '\0';
                break;
            }
            url->host = malloc(sizeof(char) * (start_port_index + 1));
            url->path = malloc(sizeof(char) * (url_buffer_size - i));
            strncpy(url->host, url_buffer, start_port_index ? start_port_index : i);
            strncpy(url->path, &(url_buffer[i + 1]), url_buffer_size - i - 1);
            break;
        }
    }
    return url;
}

int is_equal_url (const url_t* url1, const url_t* url2) {
    int is_equal = 0;
    if (url1->host != NULL && url2->host != NULL && strcmp(url1->host, url2->host) == 0
        && url1->path != NULL && url2->path != NULL && strcmp(url1->path, url2->path) == 0
        && url1->port == url2->port) {
        is_equal = 1;
    }
    return is_equal;
}

void copy_url (const url_t* url1, url_t* url2) {
    url2->host = url1->host;
    url2->path = url1->path;
    url2->port = url1->port;
}

void destroy_url (url_t* url) {
    free(url->path);
    free(url->host);
    free(url);
}