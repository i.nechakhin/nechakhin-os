#include "my_cache.h"

#define BUFFER_SIZE 1024

cache_array_t* create_cache (size_t cache_size) {
    cache_array_t* cache = malloc(sizeof(cache_array_t));
    cache->array = malloc(sizeof(cache_t) * cache_size);
    cache->size = cache_size;
    cache->free_cache_index = 0;
    for (size_t i = 0; i < cache_size; ++i) {
        cache->array[i].title = NULL;
        cache->array[i].page_size = 0;
    }
    return cache;
}

void increase_free_cache_index (cache_array_t* cache) {
    cache->free_cache_index++;
    if (cache->free_cache_index >= cache->size) {
        cache->free_cache_index = 0;
    }
}

// add new record in cache
void write_to_cache (cache_array_t* cache, int socket_fd, const url_t* url) {
    cache->array[cache->free_cache_index].title = NULL;
    increase_free_cache_index(cache);

    cache->array[cache->free_cache_index].page_size = BUFFER_SIZE;
    cache->array[cache->free_cache_index].page = malloc(BUFFER_SIZE + 1);

    size_t offset = 0;
    size_t read_bytes;
    while ((read_bytes = read(socket_fd, &((cache->array[cache->free_cache_index].page)[offset]), BUFFER_SIZE)) != 0) {
        offset += read_bytes;
        cache->array[cache->free_cache_index].page_size = offset;
        cache->array[cache->free_cache_index].page = realloc(cache->array[cache->free_cache_index].page, offset + BUFFER_SIZE + 1);
    }
    cache->array[cache->free_cache_index].page_size++;
    cache->array[cache->free_cache_index].page[cache->array[cache->free_cache_index].page_size] = '\0';

    cache->array[cache->free_cache_index].title = create_url();
    copy_url(url, cache->array[cache->free_cache_index].title);
}

// on success return index in cache array, on not found return -1
size_t lookup_cache (const cache_array_t* cache, const url_t* url) {
    for (size_t i = 0; i < cache->size; ++i) {
        if (cache->array[i].title != NULL) {
            if (is_equal_url(cache->array[i].title, url)) {
                return i;
            }
        }
    }
    return -1;
}

size_t send_from_cache (const cache_array_t* cache, int index, size_t sent_bytes, int client_fd) {
    size_t written_bytes;
    if (sent_bytes < cache->array[index].page_size) {
        size_t bytes_left = cache->array[index].page_size - sent_bytes;
        if ((written_bytes = write(client_fd, &(cache->array[index].page[sent_bytes]), BUFFER_SIZE < bytes_left ? BUFFER_SIZE : bytes_left)) > 0) {
            sent_bytes += written_bytes;
        } else if (written_bytes == (size_t) -1) {
            return -1;
        }
    } else {
        return 0;
    }
    return sent_bytes;
}

void destroy_cache (cache_array_t* cache) {
    free(cache->array);
    free(cache);
}