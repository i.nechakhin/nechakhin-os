#ifndef LAB31_MY_CACHE_H_
#define LAB31_MY_CACHE_H_

#include <stdio.h>

#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include "my_url.h"

typedef struct cache {
    size_t page_size;
    url_t* title;
    char* page;
} cache_t;

typedef struct cache_array {
    cache_t* array;
    size_t size;
    size_t free_cache_index;
} cache_array_t;

cache_array_t* create_cache (size_t cache_size);
void write_to_cache (cache_array_t* cache, int socket_fd, const url_t* url);
size_t lookup_cache (const cache_array_t* cache, const url_t* url);
size_t send_from_cache (const cache_array_t* cache, int index, size_t sent_bytes, int client_fd);
void destroy_cache (cache_array_t* cache);

#endif