#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>

void* thread_sleep (void* args) {
    char* string = (char*) args;
    unsigned int len_string = strlen(string);
    usleep(len_string * 5000);
    printf("%s\n", string);
    return NULL;
}

void check_error (int status) {
    if (status != 0) {
        printf("Error. Status: %d\n", status);
        pthread_exit(&status);
    }
}

int main (int argc, char** argv) {
    if (argc != 2) {
        perror("Error params!\n");
        return 1;
    }
    int count_strings = (int) strtol(argv[1], NULL, 0);
    pthread_t* threads = malloc(sizeof(pthread_t) * count_strings);
    char** strings = malloc((sizeof(char*) * count_strings));
    for (int i = 0; i < count_strings; ++i) {
        strings[i] = malloc(sizeof(char) * 1812);
    }
    FILE* file = fopen("text.txt", "r");
    if (file == NULL) {
        printf("ERROR: file not open!\n");
        return 1;
    }
    for (int i = 0; i < count_strings; ++i) {
        if (fgets(strings[i], 1800, file) == NULL) {
            printf("ERROR: fgets error!\n");
            return 1;
        }
        int status = pthread_create(&threads[i], NULL, thread_sleep, strings[i]);
        check_error(status);
    }
    for (int i = 0; i < count_strings; ++i) {
        pthread_join(threads[i], NULL);
        free(strings[i]);
    }
    free(strings);
    free(threads);
    return 0;
}
