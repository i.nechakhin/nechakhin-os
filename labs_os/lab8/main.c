#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

int main (void) {
    int file = open("test.txt", O_RDWR);
    if (file == -1) {
        printf("File not opening.\n");
        return 1;
    }

    struct flock flock;
    flock.l_type = F_RDLCK; //F_WRLCK
    flock.l_whence = SEEK_SET;
    flock.l_start = 0;
    flock.l_len = 0;

    if (fcntl(file, F_SETLK, &flock) == -1) {
        printf("Locked!\n");
        return 2;
    }

    system("nano text.txt");

    flock.l_type = F_UNLCK;

    fcntl(file, F_SETLK, &flock);

    return 0;
}