#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/termios.h>
#include <aio.h>

// For example:
// 1. literaturepage.com/read/doriangray-1.html
// 2. planetpublish.com/wp-content/uploads/2011/11/The_Picture_of_Dorian_Gray_NT.pdf

struct termios original_termios;

void disable () {
    tcsetattr(0, TCSAFLUSH, &original_termios);
}

void enable () {
    tcgetattr(0, &original_termios);
    atexit(disable);

    struct termios new_termios = original_termios;
    new_termios.c_lflag &= ~(ECHO | ICANON);
    tcsetattr(0, TCSAFLUSH, &new_termios);
}

int main () {
    int fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd == -1) {
        printf("ERROR: socket not open\n");
        return 1;
    }

    char full_name_host[100];
    if (scanf("%s", full_name_host) == 0) {
        printf("ERROR: scanf error\n");
        return 1;
    }

    char* token = strtok(full_name_host, "/");

    struct hostent* server = gethostbyname(token);
    if (server == NULL) {
        printf("ERROR: invalid hostname\n");
        return 1;
    }
    token = strtok(NULL, "/");

    int port = 80;
    struct sockaddr_in address;
    memset(&address, 0, sizeof(address));
    address.sin_family = AF_INET;
    address.sin_port = htons(port);
    memcpy(&address.sin_addr.s_addr, server->h_addr_list[0], server->h_length);

    if (connect(fd, (struct sockaddr*) &address, sizeof(address)) == -1) {
        printf("ERROR: connect error\n");
        return 1;
    }

    char* message = malloc(sizeof(char) * 1000);
    strcat(message, "GET ");
    while (token != NULL) {
        strcat(message, "/");
        strcat(message, token);
        token = strtok(NULL, "/");
    }
    strcat(message, " HTTP/1.0\r\nHost: ");
    token = strtok(full_name_host, "/");
    strcat(message, token);
    strcat(message, "\r\n\r\n\0");

    int len_message = strlen(message);
    if (write(fd, message, len_message) != len_message) {
        free(message);
        printf("ERROR: write error\n");
        return 1;
    }
    free(message);

    enable();

    static struct aiocb read_request;
    char read_buffer[100];
    memset(&read_request, 0, sizeof(read_request));
    read_request.aio_fildes = fd;
    read_request.aio_buf = read_buffer;
    read_request.aio_nbytes = sizeof(read_buffer);
    aio_read(&read_request);

    static struct aiocb space_request;
    char space_buffer[2];
    memset(&space_request, 0, sizeof(space_request));
    space_request.aio_fildes = 0;
    space_request.aio_buf = space_buffer;
    space_request.aio_nbytes = sizeof(space_buffer);
    aio_read(&space_request);

    int printed_lines = 0;
    char paused = 0;
    while (1) {
        if (paused) {
            static const struct aiocb* list[2] = {&space_request, NULL};
            aio_suspend(list, 1, NULL);
            size_t size = aio_return(&space_request);
            if (size > 0) {
                if (space_buffer[0] == ' ') {
                    printed_lines = 0;
                    paused = 0;
                }
                aio_read(&space_request);
            }
        } else {
            static const struct aiocb* list[2] = {&read_request, NULL};
            aio_suspend(list, 1, NULL);
            size_t size = aio_return(&read_request);
            if (size > 0) {
                if (printed_lines < 25) {
                    for (size_t i = 0; i < size; ++i) {
                        if (read_buffer[i] == '\n') {
                            printed_lines++;
                        }
                        printf("%c", read_buffer[i]);
                    }
                } else {
                    paused = 1;
                    printf("\n\nPress space to scroll down...\n");
                }
                aio_read(&read_request);
            } else if (size == 0) {
                printf("Closing connection\n");
                break;
            }
        }
    }
}