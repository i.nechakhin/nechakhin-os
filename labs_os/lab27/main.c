#include <stdio.h>

int main () {
    FILE* file = fopen("test.txt", "r");
    if (file == NULL) {
        printf("ERROR: file not open!\n");
        return 1;
    }
    FILE* pipe = popen("wc -l", "w");
    if (pipe == NULL) {
        printf("ERROR: pipe not open!\n");
        return 1;
    }

    char ch[1812];
    while (fgets(ch, 1812, file) != NULL) {
        if (ch[0] == '\n') {
            fputs(ch, pipe);
        }
    }

    pclose(pipe);
    fclose(file);
    return 0;
}
