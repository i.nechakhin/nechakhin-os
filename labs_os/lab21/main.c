#include <signal.h>
#include <stdio.h>

int count = 0;
int is_kill = 0;

void action_sigint () {
    printf("\a");
    count++;
}

void action_sigquit () {
    printf("\nThe signal sounded %d times\n", count);
    is_kill = 1;
}

int main () {
    while (1) {
        signal(SIGINT, &action_sigint);
        signal(SIGQUIT, &action_sigquit);
        if (is_kill == 1) {
            break;
        }
    }
}