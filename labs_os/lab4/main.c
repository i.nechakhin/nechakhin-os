#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct List {
    char* string;
    struct List* next;
} List;

List* new_node (List* head, char* str) {
    if (head == NULL) {
        head = malloc(sizeof(List));
        head->string = malloc(sizeof(char) * (strlen(str) + 1));
        strcpy(head->string, str);
        head->next = NULL;
        return head;
    }
    List* tmp = malloc(sizeof(List));
    tmp->string = malloc(sizeof(char) * (strlen(str) + 1));
    strcpy(tmp->string, str);
    tmp->next = head;
    return tmp;
}

List* reverse (List* head) {
    List* prev_head = NULL;
    List* tmp = NULL;
    while (head) {
        prev_head = head;
        head = head->next;
        prev_head->next = tmp;
        tmp = prev_head;
    }
    return tmp;
}

int main () {
    List* head = NULL;
    char tmp_string[1817];
    while (1) {
        fgets(tmp_string, 1817, stdin);
        if (tmp_string[0] == '.' && strlen(tmp_string) == 1) {
            break;
        }
        head = new_node(head, tmp_string);
    }
    head = reverse(head);

    printf("\nList:\n");
    List* tmp = NULL;
    while (head != NULL) {
        printf("%s\n", head->string);
        tmp = head->next;
        free(head->string);
        free(head);
        head = tmp;

    }
    return 0;
}