#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <poll.h>
#include <sys/termios.h>

// For example:
// 1. literaturepage.com/read/doriangray-1.html
// 2. planetpublish.com/wp-content/uploads/2011/11/The_Picture_of_Dorian_Gray_NT.pdf

struct termios original_termios;

void destroy_poll (struct pollfd* poll_fds) {
    for (int i = 1; i < 2; i++) {
        if (poll_fds[i].fd < 0) {
            close(poll_fds[i].fd);
        }
    }
}

void disable () {
    tcsetattr(0, TCSAFLUSH, &original_termios);
}

void enable () {
    tcgetattr(0, &original_termios);
    atexit(disable);

    struct termios new_termios = original_termios;
    new_termios.c_lflag &= ~(ECHO | ICANON);
    tcsetattr(0, TCSAFLUSH, &new_termios);
}

int main () {
    int fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd == -1) {
        printf("ERROR: socket not open\n");
        return 1;
    }

    char full_name_host[100];
    if (scanf("%s", full_name_host) == 0) {
        printf("ERROR: scanf error\n");
        return 1;
    }

    char* token = strtok(full_name_host, "/");

    struct hostent* server = gethostbyname(token);
    if (server == NULL) {
        printf("ERROR: invalid hostname\n");
        return 1;
    }
    token = strtok(NULL, "/");

    int port = 80;
    struct sockaddr_in address;
    memset(&address, 0, sizeof(address));
    address.sin_family = AF_INET;
    address.sin_port = htons(port);
    memcpy(&address.sin_addr.s_addr, server->h_addr_list[0], server->h_length);

    if (connect(fd, (struct sockaddr*) &address, sizeof(address)) == -1) {
        printf("ERROR: connect error\n");
        return 1;
    }

    char* message = malloc(sizeof(char) * 1000);
    strcat(message, "GET ");
    while (token != NULL) {
        strcat(message, "/");
        strcat(message, token);
        token = strtok(NULL, "/");
    }
    strcat(message, " HTTP/1.0\r\nHost: ");
    token = strtok(full_name_host, "/");
    strcat(message, token);
    strcat(message, "\r\n\r\n\0");

    int len_message = strlen(message);
    if (write(fd, message, len_message) != len_message) {
        free(message);
        printf("ERROR: write error\n");
        return 1;
    }
    free(message);

    struct pollfd poll_fds[2];
    poll_fds[0].fd = 0;
    poll_fds[0].events = POLLIN;
    poll_fds[1].fd = fd;
    poll_fds[1].events = POLLIN;

    enable();

    int printed_lines = 0;
    char paused = 0;
    char read_buffer[100];
    while (1) {
        if ((poll(poll_fds, 2, -1)) == -1) {
            destroy_poll(poll_fds);
            return 1;
        }

        short stdin_revents = poll_fds[0].revents;
        short socket_revents = poll_fds[1].revents;

        if (stdin_revents & POLLIN) {
            if (paused && getchar() == ' ') {
                printed_lines = 0;
                paused = 0;
            }
        }

        if (socket_revents & POLLIN) {
            if (printed_lines < 25) {
                int count_read = (int) read(fd, read_buffer, sizeof(read_buffer));
                if (count_read > 0) {
                    for (int i = 0; i < count_read; ++i) {
                        if (read_buffer[i] == '\n') {
                            printed_lines++;
                        }
                        printf("%c", read_buffer[i]);
                    }
                }

                if (count_read == -1) {
                    destroy_poll(poll_fds);
                    return 1;
                } else if (count_read == 0) {
                    printf("Closing connection\n");
                    destroy_poll(poll_fds);
                    return 0;
                }
            } else if (!paused) {
                paused = 1;
                printf("\n\nPress space to scroll down...\n");
            }
        }
    }
}
