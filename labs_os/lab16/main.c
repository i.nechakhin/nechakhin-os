#include <stdio.h>
#include <termios.h>
#include <fcntl.h>
#include <unistd.h>

int main () {
    int fd = open("/dev/tty", O_RDWR);
    struct termios terminal;
    tcgetattr(fd, &terminal);
    struct termios save_set = terminal;
    terminal.c_lflag &= ~ICANON;
    terminal.c_cc[VMIN] = 1;
    tcsetattr(fd, TCSANOW, &terminal);

    write(fd, "Do you like pizza? (y/n): ", 23);

    char ch;
    read(fd, &ch, 1);

    write(fd, "\nYou answer: ", 13);
    write(fd, &ch, 1);
    write(fd, "\n", 1);

    tcsetattr(fd, TCSAFLUSH, &save_set);

    return 0;
}