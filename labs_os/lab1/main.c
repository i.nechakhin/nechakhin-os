#include <stdio.h>
#include <stdlib.h>
#include <sys/resource.h>
#include <ulimit.h>
#include <unistd.h>

extern char** environ;

int main (int argc, char* argv[]) {
    int c;
    while ((c = getopt(argc, argv, "ispuU:cC:dvV:")) != -1) {
        if (c == 'i') {
            printf("RUID: %d\n", getuid());
            printf("EUID: %d\n", geteuid());
            printf("RGID: %d\n", getgid());
            printf("EGID: %d\n", getegid());
        } else if (c == 's') {
            setpgid(0, 0);
        } else if (c == 'p') {
            printf("Process num: %d\n", getpid());
            printf("Parent process num: %d\n", getppid());
            printf("Group process num: %d\n", getpgid(0));
        } else if (c == 'u') {
            printf("Ulimit = %ld\n", ulimit(UL_GETFSIZE, 0));
        } else if (c == 'U') {
            if (ulimit(UL_SETFSIZE, atol(optarg)) == -1) {
                printf("Bad argument!\n");
            }
        } else if (c == 'c') {
            struct rlimit lim;
            getrlimit(RLIMIT_CORE, &lim);
            printf("Core size = %ld\n", lim.rlim_cur);
        } else if (c == 'C') {
            struct rlimit lim;
            getrlimit(RLIMIT_CORE, &lim);
            lim.rlim_cur = atol(optarg);
            if (lim.rlim_cur <= lim.rlim_max) {
                setrlimit(RLIMIT_CORE, &lim);
            } else {
                printf("Bad argument!\n");
            }
        } else if (c == 'd') {
            char* buf;
            printf("DIR: %s\n", buf = getcwd(NULL, 256));
            free(buf);
        } else if (c == 'v') {
            for (int i = 0; *(environ + i); ++i) {
                printf("%s\n", *(environ + i));
            }
        } else if (c == 'V') {
            putenv(optarg);
        } else {
            printf("Bad argument!\n");
        }
    }
    return 0;
}