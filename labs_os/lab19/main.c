#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <stdbool.h>

int search (char* filename, char* template) {
    bool is_symbol = false;
    bool is_find = false;
    bool is_star = false;
    int last_index = 0;
    int count_question = 0;
    int filename_len = (int) strlen(filename);
    int template_len = (int) strlen(template);

    for (int i = 0; i < template_len; ++i) {
        switch (template[i]) {
            case '/':
                printf("Bad template format\n");
                return 1;
            case '?':
                count_question++;
                break;
            case '*':
                is_star = true;
                break;
            default:
                is_symbol = true;
                for (int j = 0; j < filename_len; ++j) {
                    if (filename[j] == template[i]) {
                        is_find = true;
                        if ((!is_star && (j - last_index) != count_question) ||
                                (is_star && (j - last_index) < count_question)) {
                            return 1;
                        }
                        j++;
                        last_index = j;
                        break;
                    }
                }
                if (!is_find) {
                    return 1;
                }
                count_question = 0;
                is_star = false;
                is_find = false;
                break;
        }
    }
    if ((!is_symbol && !is_star && filename_len != count_question) ||
            (!is_symbol && is_star && filename_len < count_question) ||
            (is_symbol && !is_star && count_question != (filename_len - last_index)) ||
            (is_symbol && is_star && (filename_len - last_index) < count_question)) {
        return 1;
    }
    return 0;
}

int main (int argc, char* argv[]) {
    if (argc == 2) {
        DIR* dirp = opendir(argv[1]);
        if (dirp == NULL) {
            return 1;
        }

        printf("Please, enter template:\n");
        char template[20];
        if (scanf("%s", template) == 0) {
            printf("ERROR: scanf error\n");
            return 1;
        }

        struct dirent* dp;
        int count_find = 0;
        while ((dp = readdir(dirp)) != NULL) {
            if (strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0) {
                continue;
            }
            if (!search(dp->d_name, template)) {
                printf("%s\n", dp->d_name);
                count_find++;
            }
        }
        if (count_find == 0) {
            printf("Template: %s\n", template);
        }

        closedir(dirp);
    } else {
        printf("ERROR: %s have bad arguments\n", argv[0]);
    }

    return 0;
}