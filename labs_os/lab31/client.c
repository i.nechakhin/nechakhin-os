#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <string.h>
#include <sys/un.h>

char* socket_path = "socket";

int main () {
    int fd = socket(AF_UNIX, SOCK_STREAM, 0);
    if (fd == -1) {
        printf("ERROR: socket not open\n");
        return 1;
    }

    struct sockaddr_un address;
    address.sun_family = AF_UNIX;
    strncpy(address.sun_path, socket_path, sizeof(address.sun_path) - 1);

    if (connect(fd, (struct sockaddr*) &address, sizeof(address)) == -1) {
        printf("ERROR: connect error\n");
        return 1;
    }

    int count_read;
    char buf[100];
    while ((count_read = read(0, buf, sizeof(buf))) > 0) {
        if (write(fd, buf, count_read) != count_read) {
            if (count_read > 0) {
                printf("Not all write\n");
            } else {
                printf("ERROR: write error\n");
                return 1;
            }
        }
    }

    return 0;
}