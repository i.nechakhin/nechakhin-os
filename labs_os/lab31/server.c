#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <string.h>
#include <ctype.h>
#include <poll.h>
#include <signal.h>

#define BACKLOG 5
#define POLL_LENGTH (BACKLOG + 1)

char* socket_path = "socket";
char is_kill;

void event_exit (int sig, siginfo_t* info, void* context) {
    if (context == NULL) {
        return;
    }
    if (sig != SIGINT || info->si_signo != SIGINT) {
        return;
    }
    unlink(socket_path);
    is_kill = 1;
}

int add_connection (struct pollfd* poll_fds, int fd) {
    int result = -1;
    for (int i = 1; i < POLL_LENGTH; i++) {
        if (poll_fds[i].fd < 0) {
            poll_fds[i].fd = fd;
            poll_fds[i].events = POLLIN | POLLPRI;
            result = 0;
            break;
        }
    }
    return result;
}

void destroy_poll (struct pollfd* poll_fds) {
    for (int i = 1; i < POLL_LENGTH; i++) {
        if (poll_fds[i].fd < 0) {
            close(poll_fds[i].fd);
        }
    }
}

int main (void) {
    int fd = socket(AF_UNIX, SOCK_STREAM, 0);
    if (fd == -1) {
        printf("ERROR: socket not open\n");
        return 1;
    }

    struct sockaddr_un address;
    memset(&address, 0, sizeof(address));
    address.sun_family = AF_UNIX;
    strncpy(address.sun_path, socket_path, sizeof(address.sun_path) - 1);

    if (bind(fd, (struct sockaddr*) &address, sizeof(address)) == -1) {
        printf("ERROR: bind error\n");
        return 1;
    }

    if (listen(fd, BACKLOG) == -1) {
        printf("ERROR: listen error\n");
        return 1;
    }

    struct pollfd poll_fds[POLL_LENGTH];
    for (int i = 0; i < POLL_LENGTH; i++) {
        poll_fds[i].fd = -1;
        poll_fds[i].events = POLLIN | POLLPRI;
    }

    poll_fds[0].fd = fd;

    struct sigaction action;
    memset(&action, 0, sizeof(action));
    action.sa_sigaction = event_exit;
    action.sa_flags = SA_SIGINFO;
    sigaction(SIGINT, &action, NULL);
    is_kill = 0;

    while (1) {
        if ((poll(poll_fds, POLL_LENGTH, -1)) == -1) {
            if (!is_kill) {
                printf("ERROR: poll error\n");
            }
            destroy_poll(poll_fds);
            return 1;
        }

        for (int i = 0; i < POLL_LENGTH; i++) {
            if (poll_fds[i].fd < 0) {
                continue;
            }
            if ((poll_fds[i].revents & POLLERR) || (poll_fds[i].revents & POLLHUP) ||
                (poll_fds[i].revents & POLLNVAL)) {
                close(poll_fds[i].fd);
                poll_fds[i].fd = -1;
                if (i == 0) {
                    if (!is_kill) {
                        printf("ERROR: server error\n");
                    }
                    destroy_poll(poll_fds);
                    return 1;
                } else {
                    printf("Closing socket\n");
                }
            }
        }

        // New connection
        if ((poll_fds[0].revents & POLLIN) || (poll_fds[0].revents & POLLPRI)) {
            int client = accept(fd, NULL, NULL);
            if (client == -1) {
                if (!is_kill) {
                    printf("ERROR: accept error\n");
                    destroy_poll(poll_fds);
                    return 1;
                }
                continue;
            }
            if (add_connection(poll_fds, client) == -1) {
                printf("Failed to add new connection\n");
            }
        }

        // Socket events
        char buf[100];
        int count_read;
        for (int i = 1; i < POLL_LENGTH; i++) {
            if (poll_fds[i].fd < 0) {
                continue;
            }
            int cur_fd = poll_fds[i].fd;
            if ((poll_fds[i].revents & POLLIN) || (poll_fds[i].revents & POLLPRI)) {
                if ((count_read = (int) read(cur_fd, buf, sizeof(buf))) > 0) {
                    for (int j = 0; j < count_read; j++) {
                        buf[j] = (char) toupper(buf[j]);
                        printf("%c", buf[j]);
                    }
                } else if (count_read == -1) {
                    if (!is_kill) {
                        printf("ERROR: read error\n");
                    }
                    destroy_poll(poll_fds);
                    return 1;
                } else if (count_read == 0) {
                    printf("Closing connection\n");
                    close(cur_fd);
                    poll_fds[i].fd = -1;
                }
            }
        }
    }
}
