#include <stdio.h>
#include <unistd.h>

int main (int argc, char* argv[]) {
    if (argc < 2) {
        printf("Bad argc\n");
        return 1;
    }

    FILE* file;

    printf("1:\n");
    printf("RUID: %d\n", getuid());
    printf("EUID: %d\n", geteuid());

    if ((file = fopen(argv[1], "r")) == NULL) {
        perror("1: File not opening\n");
        return 1;
    } else {
        printf("1: Success\n");
        fclose(file);
    }

    setuid(getuid());

    printf("2:\n");
    printf("RUID: %d\n", getuid());
    printf("EUID: %d\n", geteuid());

    if ((file = fopen(argv[1], "r")) == NULL) {
        perror("2: File not opening\n");
        return 1;
    } else {
        printf("2: Success\n");
        fclose(file);
    }

    return 0;
}