#include <sys/types.h>
#include <ctype.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

int main () {
    char* text[4] = {"Do ", "you like", " PiZZa", "?\n"};

    int pipefd[2];
    if (pipe(pipefd) == -1) {
        printf("PIPE ERROR!\n");
        return 1;
    }

    int pid = fork();
    if (pid == -1) {
        printf("FORK ERROR!\n");
        return 1;
    } else if (pid != 0) {
        close(pipefd[0]);
        for (int i = 0; i < 4; i++) {
            write(pipefd[1], text[i], strlen(text[i]));
        }
        close(pipefd[1]);
    } else {
        close(pipefd[1]);
        char ch;
        while (read(pipefd[0], &ch, 1)) {
            ch = toupper(ch);
            write(1, &ch, 1);
        }
        close(pipefd[0]);
    }

    return 0;
}