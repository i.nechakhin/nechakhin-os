#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>

int main () {
    int file = open("test.txt", O_RDONLY);

    int lines[101] = {0};
    int num = 1;
    char c;
    while (read(file, &c, 1)) {
        if (c == '\n') {
            lines[num] = lseek(file, 0, SEEK_CUR);
            num++;
        }
    }

    fd_set fd_set;
    struct timeval tv;

    char string[1817];
    int count = num;
    while (1) {
        printf("Write N of string: \n");

        FD_ZERO(&fd_set);
        FD_SET(0, &fd_set);
        tv.tv_sec = 5;
        tv.tv_usec = 0;

        int retval = select(1, &fd_set, NULL, NULL, &tv);
        if (retval == 0) {
            printf("Time is over!\n");
            lseek(file, 0, SEEK_SET);
            while (read(file, &c, 1)) {
                write(1, &c, 1);
            }
            close(file);
            return 0;
        } else if (retval == -1) {
            printf("ERROR\n");
            close(file);
            return 1;
        } else {
            char str[10];
            fgets(str, 10, stdin);
            char flag = 0;
            for (int i = 0; str[i] != '\n'; ++ i) {
                if (!isdigit(str[i])) {
                    if (i == 0 && str[i] == '-' && str[i + 1] != '\n') {
                        continue;
                    }
                    flag = 1;
                    break;
                }
            }
            if (flag) {
                printf("N should be number.\n");
            } else {
                num = atoi(str);
                if (num == 0) {
                    close(file);
                    return 0;
                }
                if (num < 0) {
                    printf("N should be > 0.\n");
                } else if (num >= count) {
                    printf("Out of range.\n");
                } else {
                    lseek(file, lines[num - 1], SEEK_SET);
                    read(file, string, lines[num] - lines[num - 1]);
                    write(1, string, lines[num] - lines[num - 1]);
                }
            }
        }
    }
}