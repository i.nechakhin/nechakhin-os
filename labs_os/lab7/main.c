#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <ctype.h>

int main () {
    int file = open("test.txt", O_RDONLY);

    struct stat file_info;
    if (fstat(file, &file_info) == -1) {
        return 1;
    }
    size_t size = file_info.st_size;

    char* pa = mmap(0, size, PROT_READ, MAP_SHARED, file, 0);

    int lines[101] = {0};
    int num = 1;
    size_t pos;
    for (pos = 0; pos < size; pos++) {
        if (*(pa + pos) == '\n') {
            lines[num] = pos + 1;
            num++;
        }
    }

    fd_set fd_set;
    struct timeval tv;

    int count = num;
    while (1) {
        printf("Write N of string: \n");

        FD_ZERO(&fd_set);
        FD_SET(0, &fd_set);
        tv.tv_sec = 5;
        tv.tv_usec = 0;

        int retval = select(1, &fd_set, NULL, NULL, &tv);;
        if (retval == 0) {
            printf("Time is over!\n");
            write(1, pa, size);
            close(file);
            return 0;
        } else if (retval == -1) {
            printf("ERROR\n");
            close(file);
            return 1;
        } else {
            char str[10];
            fgets(str, 10, stdin);
            char flag = 0;
            for (int i = 0; str[i] != '\n'; ++ i) {
                if (!isdigit(str[i])) {
                    if (i == 0 && str[i] == '-' && str[i + 1] != '\n') {
                        continue;
                    }
                    flag = 1;
                    break;
                }
            }
            if (flag) {
                printf("N should be number.\n");
            } else {
                num = atoi(str);
                if (num == 0) {
                    close(file);
                    return 0;
                }
                if (num < 0) {
                    printf("N should be > 0.\n");
                } else if (num >= count) {
                    printf("Out of range.\n");
                } else {
                    write(1, pa + lines[num - 1], lines[num] - lines[num - 1]);
                }
            }
        }
    }
}