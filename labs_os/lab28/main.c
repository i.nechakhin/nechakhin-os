#include <libgen.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <time.h>

int main () {
    FILE* fp[2];
    p2open("sort -n", fp);

    srand(time(0));
    for (int i = 0; i < 100; ++ i) {
        fprintf(fp[0], "%2d\n", rand() % 100);
    }
    fclose(fp[0]);

    char ch[4];
    for (int i = 0; i < 100; ++ i) {
        fgets(ch, 4, fp[1]);
        printf("%c%c ", ch[0], ch[1]);
        if ((i + 1) % 10 == 0) {
            printf("\n");
        }
    }
    fclose(fp[1]);
    return 0;
}
