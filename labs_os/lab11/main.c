#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <wait.h>

extern char** environ;

int execvpe (char* filename, char* arg[], char* envp[]) {
    environ = envp;

    printf("New environ:\n");
    for (char** ptr = environ; *ptr != NULL; ptr++) {
        printf("%s\n", *ptr);
    }

    execvp(filename, arg);
    return -1;
}

int main (int argc, char* argv[]) {
    pid_t pid = fork();
    if (pid == -1) {
        printf("Child process not init.\n");
        return 1;
    } else if (pid == 0) {
        if (argc > 1) {
            char* envp[] = {"TZ=America/Los_Angeles", "LANG=C"};
            execvpe(argv[1], &argv[1], envp);
        }
        return 1;
    } else {
        if (wait(NULL) != -1) {
            printf("\nProcess (PID: %d) finished.\n", pid);
        }
        return 0;
    }
}