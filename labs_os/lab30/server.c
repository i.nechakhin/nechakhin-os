#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>

char* socket_path = "socket";
char is_kill;

void event_exit (int sig, siginfo_t* info, void* context) {
    if (context == NULL) {
        return;
    }
    if (sig != SIGINT || info->si_signo != SIGINT) {
        return;
    }
    unlink(socket_path);
    is_kill = 1;
}

int main (void) {
    int fd = socket(AF_UNIX, SOCK_STREAM, 0);
    if (fd == -1) {
        printf("ERROR: socket not open\n");
        return 1;
    }

    struct sockaddr_un address;
    memset(&address, 0, sizeof(address));
    address.sun_family = AF_UNIX;
    strncpy(address.sun_path, socket_path, sizeof(address.sun_path) - 1);

    if (bind(fd, (struct sockaddr*) &address, sizeof(address)) == -1) {
        printf("ERROR: bind error\n");
        return 1;
    }

    if (listen(fd, 5) == -1) {
        printf("ERROR: listen error\n");
        return 1;
    }

    struct sigaction action;
    memset(&action, 0, sizeof(action));
    action.sa_sigaction = event_exit;
    action.sa_flags = SA_SIGINFO;
    sigaction(SIGINT, &action, NULL);
    is_kill = 0;

    int count_read;
    char read_buffer[100];
    while (1) {
        int client = accept(fd, NULL, NULL);
        if (client == -1) {
            if (!is_kill) {
                printf("ERROR: accept error\n");
            }
            return 1;
        }

        while ((count_read = (int) read(client, read_buffer, sizeof(read_buffer))) > 0) {
            for (int i = 0; i < count_read; ++i) {
                read_buffer[i] = (char) toupper(read_buffer[i]);
                printf("%c", read_buffer[i]);
            }
        }

        if (count_read == -1) {
            if (!is_kill) {
                printf("ERROR: read error\n");
            }
            close(client);
            return 1;
        } else if (count_read == 0) {
            printf("Close connection\n");
            close(client);
        }
    }
}