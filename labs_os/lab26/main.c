#include <stdio.h>

int main () {
    char* text[4] = {"Do ", "you like", " PiZZa", "?\n"};

    FILE* pipe = popen("./up_all", "w");
    if (pipe == NULL) {
        printf("ERROR: pipe not open!\n");
        return 1;
    }

    for (int i = 0; i < 4; ++ i) {
        fputs(text[i], pipe);
    }

    pclose(pipe);
    return 0;
}
