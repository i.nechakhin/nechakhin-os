#include <stdio.h>
#include <unistd.h>
#include <ctype.h>

int main () {
    char ch;
    while (read(0, &ch, 1)) {
        ch = toupper(ch);
        if (write(1, &ch, 1) == -1) {
            return 1;
        }
    }
    return 0;
}

