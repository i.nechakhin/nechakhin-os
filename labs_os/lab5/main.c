#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

int main () {
    int file = open("test.txt", O_RDONLY);

    int lines[101] = {0};
    int num = 1;
    char c;
    while (read(file, &c, 1)) {
        if (c == '\n') {
            lines[num] = lseek(file, 0, SEEK_CUR);
            num ++;
        }
    }
    char string[1817];
    int count = num;
    while (1) {
        printf("Write N - number of string: ");
        scanf("%d", &num);
        if (num == 0) {
            close(file);
            return 0;
        }
        if (num < 0) {
            printf("N should be > 0.\n");
        } else if (num >= count) {
            printf("Out of range.\n");
        } else {
            lseek(file, lines[num - 1], SEEK_SET);
            read(file, string, lines[num] - lines[num - 1]);
            write(1, string, lines[num] - lines[num - 1]);
        }
    }
}
