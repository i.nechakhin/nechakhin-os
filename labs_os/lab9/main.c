#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>

int main (void) {
    pid_t pid = fork();
    if (pid == -1) {
        printf("Child process not init.\n");
        return 1;
    } else if (pid == 0) {
        execl("/bin/cat", "cat", "test.txt", NULL);
    } else {
        printf("Parent waits...\n");
        wait(NULL);
        printf("Child process finished.\n PID: %d.\n", pid);
    }
    return 0;
}