#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <wait.h>

int main (int argc, char* argv[]) {
    pid_t pid = fork();
    if (pid == -1) {
        printf("Child process not init.\n");
        return 1;
    } else if (pid == 0) {
        if (argc > 1) {
            execvp(argv[1], &argv[1]);
        }
    } else {
        int status;
        wait(&status);
        if (WIFEXITED(status)) {
            printf("\nProcess (PID: %d) finished with status: %d.\n", pid, WEXITSTATUS(status));
        } else if (WIFSIGNALED(status)) {
            printf("\nProcess (PID: %d) finished with error signal: %d.\n", pid, WTERMSIG(status));
        } else if (WIFSTOPPED(status)) {
            printf("\nProcess (PID: %d) finished with signal: %d.\n", pid, WSTOPSIG(status));
        }
        return 0;
    }
}