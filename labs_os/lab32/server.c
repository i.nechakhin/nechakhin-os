#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdlib.h>
#include <strings.h>
#include <ctype.h>
#include <aio.h>
#include <signal.h>

char* socket_path = "socket";
char is_kill;

void event_action (int sig, siginfo_t* info, void* context) {
    if (context == NULL) {
        return;
    }
    if (sig != SIGIO && sig != SIGINT) {
        return;
    }
    if (sig == SIGINT) {
        unlink(socket_path);
        is_kill = 1;
        return;
    }
    struct aiocb* request = info->si_value.sival_ptr;
    if (aio_error(request) == 0) {
        size_t size = aio_return(request);
        char* buffer = (char*) request->aio_buf;

        if (size == 0) {
            printf("Closing connection\n");
            close(request->aio_fildes);
            free(buffer);
            free(request);
            return;
        }

        for (size_t i = 0; i < size; i++) {
            int c = toupper(buffer[i]);
            printf("%c", c);
        }

        // Schedule next read
        aio_read(request);
    }
}

int main () {
    int fd = socket(AF_UNIX, SOCK_STREAM, 0);
    if (fd == -1) {
        printf("ERROR: socket not open\n");
        return 1;
    }

    struct sockaddr_un address;
    memset(&address, 0, sizeof(address));
    address.sun_family = AF_UNIX;
    strncpy(address.sun_path, socket_path, sizeof(address.sun_path) - 1);

    if (bind(fd, (struct sockaddr*) &address, sizeof(address)) == -1) {
        printf("ERROR: bind error\n");
        return 1;
    }

    if (listen(fd, 5) == -1) {
        printf("ERROR: listen error\n");
        return 1;
    }

    struct sigaction action;
    memset(&action, 0, sizeof(action));
    action.sa_sigaction = event_action;
    action.sa_flags = SA_SIGINFO;
    sigaction(SIGIO, &action, NULL);
    sigaction(SIGINT, &action, NULL);

    while (1) {
        int client = accept(fd, NULL, NULL);
        if (client == -1) {
            if (is_kill) {
                return 1;
            }
            continue;
        }

        struct aiocb* request = calloc(1, sizeof(struct aiocb));
        request->aio_fildes = client;
        request->aio_buf = malloc(sizeof(char) * 100);
        request->aio_nbytes = 100;
        request->aio_sigevent.sigev_notify = SIGEV_SIGNAL;
        request->aio_sigevent.sigev_signo = SIGIO;
        request->aio_sigevent.sigev_value.sival_ptr = request;

        if (aio_read(request) == -1) {
            if (!is_kill) {
                printf("ERROR: aio_read error\n");
            }
            close(client);
            return 1;
        }
    }
}